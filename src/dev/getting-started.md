# Getting Started

Veloren is written in the Rust programming language and is maintained using Git, the industry standard version control system.

## Why Rust?

Rust is a new programming language designed to be fast, safe, and well-suited to concurrent programming. We believe that these attributes make it ideal for game design. As a result, the Veloren engine is extremely quick, very rugged (you'll be hard-pressed to find unintentional crashes or bugs in the codebase), and also multi-threaded (meaning it can take advantage of multi-core CPU architectures).

## Why Git?

Git is an industry standard version control system. It is uses to keep track of different versions of a codebase, and maintains a history of a codebase that can be viewed throughout time. It also makes contributing to the project much easier, since it helps to manage the merging and updating of the codebases maintained by each contributor.

# Installing things

To get started working on Veloren, you'll need to install a few things.

## Rust

Rust can be easily installed on all major desktop operating systems. Follow this link for installation instructions specific to your system.

[https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install)

## Git

There are many ways to install Git. For those running Linux, your system probably has Git already installed or available to install with your chosen package manager.

For Windows, The ['Git for Windows'](https://gitforwindows.org/) suite is a sensible way to install Git, along with a set of tools that'll make it easier for you to use.

*For Mac OS, there exists other methods. I personally have not used Mac OS, so you'll likely have a better time finding instructions just by googling 'Install Git on Mac OS'. If someone wants to contribute appropriate instructions to this page, please do.*

# GitLab and forking Veloren

By now, you should have both Rust and Git installed.
