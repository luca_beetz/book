# What is Veloren?

Veloren is a multiplayer voxel RPG written in Rust. It is inspired by games such as Cube World,
Legend of Zelda: Breath of the Wild, Dwarf Fortress and Minecraft.

![A sunset over a desert town](../images/desert-sunset.png)

Veloren is fully open-source, licensed under GPL 3. It uses original graphics, musics and other
assets created by its volunteer community. Being contributor-driven, its development community
and user community is one and the same: developers, players, artists and musicians come together
to develop the game.

## What status is the project currently in?

Veloren is currently undergoing a from-scratch rewrite. The old engine is playable and is rather
fun to explore, although all new development work is focussed on the new engine.

## [Contribute to this book](https://gitlab.com/veloren/book)
